/*
 * See documentation at https://nRF24.github.io/RF24
 * See License information at root directory of this library
 * Author: Brendan Doherty (2bndy5)
 */

#define LCD_RS  9
//#define LCD_RW
#define LCD_EN  8
#define LCD_D4  5
#define LCD_D5  4
#define LCD_D6  3
#define LCD_D7  2

#define ENC_1   A1    // Encoder Pin1
#define ENC_2   A0    // Encoder Pin2




#define USE_TIMER_1     true
#define USE_TIMER_2     true




#include <SPI.h>
#include <LiquidCrystal.h>
#include "printf.h"
#include "RF24.h"
#include "TimerInterrupt.h"


// instantiate an object for the nRF24L01 transceiver
RF24 radio(7, 10); // using pin 7 for the CE pin, and pin 8 for the CSN pin
LiquidCrystal lcd(LCD_RS, LCD_EN, LCD_D4, LCD_D5, LCD_D6, LCD_D7); 

// Let these addresses be used for the pair
uint8_t address[][6] = {"1Node", "2Node"};
// It is very helpful to think of an address as a path instead of as
// an identifying device destination

// to use different addresses on a pair of radios, we need a variable to
// uniquely identify which address this radio will use to transmit
bool radioNumber = 1; // 0 uses address[0] to transmit, 1 uses address[1] to transmit

// Used to control whether this node is sending or receiving
bool role = false;  // true = TX role, false = RX role

// For this example, we'll be using a payload containing
// a single float number that will be incremented
// on every successful transmission
float payload = 0.0;

volatile int8_t enc_delta=0;          // -128 ... 127
static int8_t   last_val;

void setup() {
  pinMode(ENC_1, INPUT_PULLUP);
  pinMode(ENC_2, INPUT_PULLUP);
  lcd.begin(16, 2);

  lcd.print("Hello");
  lcd.setCursor(0,1);
  lcd.print("Vol: ");
  
  Serial.begin(115200);
  while (!Serial) {
  }
  Serial.println("\nsimpleRadio!\n");

  // initialize the transceiver on the SPI bus
  if (!radio.begin()) {
    Serial.println(F("radio hardware is not responding!!"));
    while (1) {} // hold in infinite loop
  }

  radio.setPALevel(RF24_PA_LOW);  // RF24_PA_MAX is default.

  radio.setPayloadSize(sizeof(payload)); // float datatype occupies 4 bytes

  // set the TX address of the RX node into the TX pipe
  radio.openWritingPipe(address[1]);     // always uses pipe 0

  // set the RX address of the TX node into a RX pipe
  radio.openReadingPipe(1, address[0]); // using pipe 1

  // additional setup specific to the node's role
  if (role) {
    radio.stopListening();  // put radio in TX mode
  } else {
    radio.startListening(); // put radio in RX mode
  }

  ITimer1.init();
  if (ITimer1.attachInterruptInterval(1UL, TimerHandler))   // Intervall in ms
  {
    Serial.print(F("Starting  ITimer1 OK, millis() = ")); Serial.println(millis());
  }
  else
    Serial.println(F("Can't set ITimer1. Select another freq. or timer"));

  // For debugging info
  printf_begin();             // needed only once for printing details
  //radio.printDetails();       // (smaller) function that prints raw register values
  radio.printPrettyDetails(); // (larger) function that prints human readable data
} // setup

int8_t encoder_value=0;
int8_t encoder_value_old=0;

void loop() {
  
  encoder_value += encode_read(4)*2;
  if(encoder_value != encoder_value_old){
    if(encoder_value<0)  encoder_value=0;
    if(encoder_value>100) encoder_value=100;
    Serial.println(encoder_value);
    lcd.setCursor(5,1);
    lcd.print("   ");
    lcd.setCursor(5,1);
    lcd.print(encoder_value);

    encoder_value_old=encoder_value;
  }


  if (role) {
    // This device is a TX node

    unsigned long start_timer = micros();                    // start the timer
    bool report = radio.write(&payload, sizeof(float));      // transmit & save the report
    unsigned long end_timer = micros();                      // end the timer

    if (report) {
      Serial.print(F("Transmission successful! "));          // payload was delivered
      Serial.print(F("Time to transmit = "));
      Serial.print(end_timer - start_timer);                 // print the timer result
      Serial.print(F(" us. Sent: "));
      Serial.println(payload);                               // print payload sent
      payload += 0.01;                                       // increment float payload
    } else {
      Serial.println(F("Transmission failed or timed out")); // payload was not delivered
    }

    // to make this example readable in the serial monitor
    delay(1000);  // slow transmissions down by 1 second

  } else {
    // This device is a RX node

    uint8_t pipe;
    if (radio.available(&pipe)) {             // is there a payload? get the pipe number that recieved it
      uint8_t bytes = radio.getPayloadSize(); // get the size of the payload
      radio.read(&payload, bytes);            // fetch payload from FIFO
      Serial.print(F("Received "));
      Serial.print(bytes);                    // print the size of the payload
      Serial.print(F(" bytes on pipe "));
      Serial.print(pipe);                     // print the pipe number
      Serial.print(F(": "));
      Serial.println(payload);                // print the payload's value
    }
  } // role

  if (Serial.available()) {
    // change the role via the serial monitor

    char c = toupper(Serial.read());
    if (c == 'T' && !role) {
      // Become the TX node

      role = true;
      Serial.println(F("*** CHANGING TO TRANSMIT ROLE -- PRESS 'R' TO SWITCH BACK"));
      radio.stopListening();

    } else if (c == 'R' && role) {
      // Become the RX node

      role = false;
      Serial.println(F("*** CHANGING TO RECEIVE ROLE -- PRESS 'T' TO SWITCH BACK"));
      radio.startListening();
    }
  }

} // loop


void TimerHandler(void)
{
  int8_t new_val, diff, tmp;

  new_val = 0;
  if (!digitalRead(ENC_1)) new_val  = 3;
  if (!digitalRead(ENC_2)) new_val ^= 1;   // convert gray to binary
  diff = last_val - new_val;               // difference last - new
  if( diff & 1 ) {                         // bit 0 = value (1)
    last_val = new_val;                    // store new as next last
    enc_delta += (diff & 2) - 1;           // bit 1 = direction (+/-)
  }
}

// read 1, 2, or 4 step encoders
int8_t encode_read( uint8_t step )    { 
  int8_t val;

  // atomic access to enc_delta
  cli();
  val = enc_delta;
  switch (step) {
    case  2: enc_delta = val & 1; val >>= 1; break;
    case  4: enc_delta = val & 3; val >>= 2; break;
    default: enc_delta = 0; break;
  }
  sei();
  return val;                   // counts since last call
}
