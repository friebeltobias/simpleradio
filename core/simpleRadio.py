import sys
import struct
from RF24 import RF24, RF24_PA_LOW

radio = RF24(22, 0)	# CE Pin, CS Pin

payload = [0.0]

# initialize the nRF24L01 on the spi bus
address = [b"1Node", b"2Node"]

if not radio.begin():
  raise RuntimeError("radio hardware is not responding")

radio.setPALevel(RF24_PA_LOW)  # RF24_PA_MAX is default
radio.openWritingPipe(address[0])  # always uses pipe 0
radio.openReadingPipe(1, address[1])  # using pipe 1
radio.payloadSize = len(struct.pack("<f", payload[0]))

while(1):
  radio.startListening()  # put radio in RX mode

  has_payload, pipe_number = radio.available_pipe()
  if has_payload:
    buffer = radio.read(radio.payloadSize)
    payload[0] = struct.unpack("<f", buffer[:4])[0]
    print(
      "Received {} bytes on pipe {}: {}".format(
        radio.payloadSize,
        pipe_number,
        payload[0]
      )
    )
